import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Todo List';
  todos = [
    { label:'Bring Milk', 
    done: false, 
   
  },

  { label:'Pay mobile bill', 
    done: true, 
   
  },

  { label:'Clean house', 
    done: false, 
   
  },

  { label:'Fix the bulb', 
    done: false, 
    
  },



];

addTodo(newTodoLabel) {

  var newTodo = {
    label: newTodoLabel,
    priority: 1,
    done: false
  };

  this.todos.push(newTodo);  

}

deleteTodo(todo){
  this.todos = this.todos.filter( t =>t.label !== todo.label );
}

}
